#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
// Definición de variables y archivos para obtención de claves:
#define NUMERO_1 123
#define NUMERO_2 124
#define ARCHIVO_1 "archivo_A.txt"
#define ARCHIVO_2 "archivo_B.txt"
// Declaración de manejadores de señales:
void handler_sigterm(int);
void handler_sigusr1(int);
void handler_sigusr2(int);
// Flags globales que se utilizan en los manejadores de señales:
int flag1 = 0;
int flag2 = 0;
int flag3 = 0;


int main( int argc, char **argv)
{
    int Id_Memoria1, Id_Memoria2;   // Ids para regiones de memoria compartida.
    key_t clave_1, clave_2;         // Claves para regiones de memoria compartida.
    int *Memoria1 = NULL;           // Puntero para manejar región de memoria 1.
    pid_t *Memoria2 = NULL;         // Puntero para manejar región de memoria 2.
    int i,j;                        // Indices para bucles for.
    int arreglo_enteros[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19}; // Arreglo de 20 enteros.
    pid_t pid_A, pid_B;             // Variables para almacenar PID de los procesos A y B.
    int flag4 = 1;                  // Flag que se usa para envíar solo una vez la señal al otro proceso.
    
    // Establecimiento de los manejadores de señales:
    signal(SIGTERM, handler_sigterm);
    signal(SIGUSR1, handler_sigusr1);
    signal(SIGUSR2, handler_sigusr2);
    // Obtención de claves para regiones de memoria:
    clave_1 = ftok(ARCHIVO_1, NUMERO_1);
    clave_2 = ftok(ARCHIVO_2, NUMERO_2);
    // Obtención de Ids para regiones de memoria utilizando las claves:
    Id_Memoria1 = shmget (clave_1, 20*sizeof(int), 0666 | IPC_CREAT | IPC_EXCL);
    if(Id_Memoria1 == -1)
    {
        Id_Memoria1 = shmget (clave_1, 20*sizeof(int), 0666);
    }
    Id_Memoria2 = shmget (clave_2, 2*sizeof(pid_t), 0666 | IPC_CREAT | IPC_EXCL);
    if(Id_Memoria2 == -1)
    {
        Id_Memoria2 = shmget (clave_2, 2*sizeof(pid_t), 0666);
    }
    // Asociación de las regiones de memoria a los punteros Memoria1 y Memoria2 utilizando los Ids:
    Memoria1 = (int *)shmat (Id_Memoria1, (const void *)0, 0);
    Memoria2 = (pid_t *)shmat (Id_Memoria2, (const void *)0, 0);

    if (argv[1][0] == 'A') // Se ingresó al programa como proceso A.
    {
        pid_A = getpid(); 
        printf("PID proceso A: %d\n", pid_A);
    
        Memoria2[0] = pid_A; // Guardo PID de A en zona de memoria compartida. 
        
        while(flag1 != 1) // Proceso se repite indefinidamente hasta que se recibe la señal SIGTERM.
        {
            for(i=0;i<10;i++) // Escritura de 10 primeros numeros por programa A.
            {
                Memoria1[i] = (arreglo_enteros[i]*4 + 5) % 27;
                sleep(1);  
            }
            while(1) // Espera a que se cree B.
            {   
                if(Memoria2[1] > 0 && flag4 == 1) // Significa que B ya se creó y guardó su PID en Memoria2[1].
                {
                    kill(Memoria2[1], SIGUSR1); // Le indico a proceso B que A ya terminó de escribir.
                    flag4 = 0;  // Flag para asegurar de que solamente mando una vez la señal SIGUSR1 a B.
                }
                if(flag3 == 1) // Si se cumple significa que B le mando la señal SIGUSR2 que indica que ya terminó de escribir.
                {
                    flag4 = 1;
                    break; // Sale del while para empezar a leer.
                }
            }
            if(flag3 == 1) // B le mando la señal que terminó de escribir, por ende A puede leer.
            {
                printf("Proceso A leyendo\n");
                for(j=10;j<20;j++) // Lectura de 10 segundos numeros por programa A.
                {
                    printf("%d\n", (Memoria1[j]*7+19) % 27);
                }
                flag3=0;
            }
        }
        // Se recibió la señal SIGTERM por ende se salió del while:
        printf("Señal SIGTERM recibida\n");
        kill(Memoria2[1], SIGKILL); // Mato proceso B.
        shmdt(Memoria1); // Libero recursos.
        shmctl (Id_Memoria1, IPC_RMID, (struct shmid_ds *)NULL); // Borro espacio de memoria.
        shmdt(Memoria2); // Libero recursos.
        shmctl (Id_Memoria2, IPC_RMID, (struct shmid_ds *)NULL); // Borro espacio de memoria.
        kill(pid_A, SIGKILL); // Mato proceso A.
    }
    else if (argv[1][0] == 'B') // Se ingresó al programa como proceso B.
    {
        pid_B = getpid();
        Memoria2[1] = pid_B; // Guardo PID de B en zona de memoria compartida.
       
        while(1) // Proceso se repite indefinidamente hasta que A mate a B.
        {
            for(i=10;i<20;i++) // Escritura de 10 segundos numeros por programa B.
            {
                Memoria1[i] = (arreglo_enteros[i]*4 + 5) % 27;
                sleep(1);
            }
        
            while(1) // Espera a que se cree A.
            {
                if(Memoria2[0] > 0 && flag4 == 1) // Significa que A ya se creó y guardó su PID en Memoria2[0].
                {
                    kill(Memoria2[0], SIGUSR2); // Le indico a proceso A que B ya terminó de escribir.
                    flag4 = 0;  // Flag para asegurar que solamente mando una vez la señal SIGUSR2 a A.
                }
                if(flag2 == 1) // Si se cumple significa que A le mandó la señal SIGUSR1 que indica que ya terminó de escribir.
                {
                    flag4 = 1;
                    break; // Sale del while para empezar a leer.
                }
            }

            if(flag2 == 1) // A le mando la señal que terminó de escribir, por ende B puede leer.
            {
                printf("Proceso B leyendo\n");
                for(j=0;j<10;j++) // Lectura de 10 primeros numeros por programa B.
                {
                    printf("%d\n", (Memoria1[j]*7+19) % 27);
                }
                flag2 = 0;
            }
        }
    }
}

void handler_sigterm(int sig) // Flag para señal SIGTERM.
{
    flag1 = 1;
}

void handler_sigusr1(int sig) // Flag para señal SIGUSR1.
{
    flag2 = 1;
}

void handler_sigusr2(int sig) // Flag para señal SIGUSR2.
{
    flag3 = 1;
}