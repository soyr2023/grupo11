#!/bin/bash 
dire="$1"
# Verificar que el argumento pasado en la línea de comandos es efectivamente un directorio
if [ ! -d "$dire" ]; # Verifica si el primer argumento pasado en la linea de comandos es un directorio
 then
    echo "El argumento pasado no es un directorio" # Si no es un directorio, imprimo el mensaje y termino la ejecucion
    exit 1
fi

# Revisar el contenido del directorio
dir_cont=0
archi_cont=0
otros_cont=0

for vari in "$dire"/*; # Recorro todos los elementos del directorio
do
    if [ -d "$vari" ]; # Verifico si es un directorio
    then
        ((dir_cont++)) # Si lo es, suma en esta variable
    elif [ -f "$vari" ]; # Verifico si es un archivo
    then
        ((archi_cont++)) # Si lo es, suma en esta variable
    else
        ((otros_cont++)) # Si no es ni directorio, ni archivo, suma en esta variable que es otro
    fi
done

# Imprimir resultados
echo "Cantidad de directorios: $dir_cont"
echo "Cantidad de archivos regulares: $archi_cont"
echo "Cantidad de archivos que no son regulares ni son directorios: $otros_cont"