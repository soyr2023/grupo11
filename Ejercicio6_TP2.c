#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

// Declaración de manejadores de señales
void handler_sigusr1(int);
void handler_sigusr2(int);
void handler_sigterm(int);
//---------------------------------------//

pid_t var=0;  // Variables globales para guardar los valores de los PID devueltos por fork.
pid_t var2=0;
int hijos=0; // Numero de procesos hijos creados.
pid_t vector_hijos[100];  // Guarda los PID de los hijos en un vector.

int flag1 = 0;  // Banderas para verificar que se recibieron ciertas señales.
int flag2 = 0;
int flag3 = 0;

int main (void)
{
    
    signal(SIGUSR1, handler_sigusr1); // Manejadores de las señales.
    signal(SIGUSR2, handler_sigusr2);
    signal(SIGTERM, handler_sigterm); 
    printf("Soy el padre, mi PID es: %d\n", getpid()); // Imprime el PID del padre.
    
    while(1) // Entrada al bucle.
    {
        if(flag1==1) // Significa que se recibió la señal SIGUSR1.
        {   
            flag1 = 0; // Restablece la flag a 0.
            var = fork(); // Crea un proceso hijo.
        
            if (var == -1) //Manejo de errores para la función fork().
            {
                perror("fork failed");  // Si fork() devuelve -1, entonces imprime un mensaje de error.
                exit(EXIT_FAILURE);	    // Y termina el programa con un estado de fallo, EXIT_FAILURE.
            }
            else if (var != 0)  // Verifica si el código se está ejecutando en el proceso padre.
            {
                vector_hijos[hijos] = var; // Modifica var que le devuelve PID del hijo al padre.
                hijos++;
            }

            if(var == 0) // Verifica si el código se está ejecutando en el proceso hijo.
            {
                while(1)
                {
                    printf("Soy hijo PID: %d y mi padre tiene PID: %d\n", getpid(),getppid());
                    sleep(5); // Retardo de 5 segundos.
                }
            }
        } else if (flag2==1) // Significa que se recibió la señal SIGUSR2.
        { 
            flag2 = 0;      // Restablece la flag a 0.
            var2 = fork();  // Crea un proceso hijo.
            if(var2 == -1)  // Manejo de errores para la función fork().
            {
                perror("fork failed");
                exit(EXIT_FAILURE);
            }
            else if(var2 == 0) // Verifica si el código se está ejecutando en el proceso hijo.
            {
                printf("Soy el hijo, mi PID es: %d\n", getpid());
                execl("/bin/ls", "ls", NULL);   // Ejecutará el programa ls, que está ubicado en el directorio /bin/.
                exit(0);                        // Termina el proceso hijo con un estado de éxito.
            }
    
        } else if (flag3==1) // Significa que se recibió la señal SIGTERM.
        {
            flag3 = 0;  // Restablece la flag a 0.
            for(int i=0;i<hijos;i++)  // Entra al bucle y lo recorre tantas veces como hijos creados haya.
            {
                kill(vector_hijos[i], SIGKILL); // Envia la señal para terminar con el proceso hijo.
                printf("Proceso hijo terminado, PID: %d\n",vector_hijos[i]);
                wait(NULL); // Agregado para limpiar los procesos hijos terminados, el padre espera a que termine el hijo y evita que se convierta en un proceso zombi.
                sleep(1); // Duerme 1 seg al proceso padre, para seguir con el  suigiente hijo en el bucle.
            }
            printf("Proceso padre con PID: %d ,terminado\n", getpid());
            
            exit(0); //Termina el proceso padre con un estado de éxito
        }
    }
}

// Estas funciones simplemente establecen una flag global a 1 para indicar que se ha recibido la señal correspondiente.

void handler_sigusr1(int sig) // FLAG CREACION DE HIJOS.
{
    flag1 = 1;
}

void handler_sigterm(int sig) // FLAG ALARMA DEL PADRE.
{
    flag3 = 1;
}

void handler_sigusr2(int sig) // FLAG MATAR PROCESOS HIJOS.
{
    flag2 = 1;
}