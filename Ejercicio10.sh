#!/bin/bash
#El directorio debe ser pasado como /Directorio/ (La ruta completa)

directorio="$1" # Guarda dirección en una variable llamada directorio.

# If para chequear si el argumento ingresado es un directorio, sino sale del programa.
if [ -d "$directorio" ];
then
        echo "El argumento ingresado es un directorio"
else
        echo "El argumento ingresado no es un directorio"
	exit 1
fi
#------------------------------------------------------------------------------------

# Cuenta directorios listando los archivos con ls -la y contando con wc -l las líneas que empiezan con d (indicado con grep):
resultado=$(ls -la  $directorio | grep '^d' | wc -l)
echo "Número de directorios en el directorio especificado (incluye ocultos): $resultado"

# Cuenta archivos regulares listando los archivos con ls -la y contando con wc -l las líneas que empiezan con - (indicado por grep):
resultado=$(ls -la  $directorio | grep '^-' | wc -l)
echo "Número de archivos regulares en el directorio especificado (incluye ocultos): $resultado"

# Cuenta archivos no regulares listando los archivos con ls -la y contando con wc -l las líneas que no comienzan ni con d ni con -
# (indicado por grep), luego resta uno al resultado para que no se tenga en cuenta la primera línea de ls -la que indica la cantidad
# total de archivos:
resultado=$(ls -la $directorio | grep -vE '^(-|d)' | wc -l)
resultado=$((resultado-1))
echo "Número de archivos que no son regulares ni directorios: $resultado"

exit 0
