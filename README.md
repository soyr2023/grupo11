# GRUPO11


## TP 1: Ejercicio 10 - Resumen

El ejecutable es un script que al pasarle la dirección de un directorio, verifica que lo sea para luego contar la cantidad de directorios, archivos regulares y archivos no regulares que se encuentran en dicho directorio y mostrar los resultados en pantalla.

## TP 1: Ejercicio 10 - Comandos utilizados

1s -la: Lista los archivos y directorios incluidos en el directorio que se le pase como argumento, mostrando información detallada sobre ellos, incluyendo los archivos ocultos.

grep: Busca patrones de texto dentro de archivos. 
- Con la combinación: ^ Grep busca el patrón que se especifique después del mismo.
- Con: -v Se muestran las líneas que no coinciden con el patrón que hemos buscado.
- Con: -E Se le indica a grep que interprete el patrón como una expresión regular extendida, lo que permite el uso de operadores adicionales como |, +, y ().
- Con: -vE Se realiza la busca en un archivo o entrada de texto y muestra las líneas que no coinciden con una expresión regular extendida especificada.

wc -l: Cuenta las líneas del archivo. 
- Con wc cuenta palabras,líneas y caracteres de un archivo o entrada de texto; y al agregar -l solo cuenta las líneas

echo: Imprime texto en pantalla.
- Con: “” Para encerrar el texto a imprimir y conservar los espacios en blanco.

| : Envía la salida de un comando como entrada a otro comando.

$: Accede al valor de una variable.

-d: Directorio o carpeta y si existe.

(-) o f : Archivo normal.

exit 0: Para saber que no existe un error en el código.

exit 1 u otro valor distinto de 0: Para saber que existe un error al correr el código.

Condicional (if): Para el control del flujo.

touch: Crear un fichero vacío, si existe, le modifica fecha y hora con la del sistema.

cd: Acceder y moverse por carpetas.

./: Para ejecutar el script

ls -l: Para verificar permisos e información 

chmod ugo ± rwx: Cambia los permisos de un archivo 
- Se utiliza: ± Para poner y sacar permisos. 
- u: Usuario, dueño.
- g:  Grupos.
- o: Otros (que no es el propietario, no en el mismo grupo del archivo).
- a: Todos.
- r: Lectura. 
- w: Escritura. 
- x: Ejecución.


## TP 1: Ejercicio 10 - Funcionamiento

Se utilizó un if para verificar si la ruta pasada se trata de un directorio o no, luego con el comando ls -la se obtiene la lista de archivos y directorios (ocultos y visibles) y con el uso del pipeline se le pasa la lista al comando grep. Con este comando se buscan en la lista los archivos (empiezan con -), los directorios (empiezan con d) y los que no cumplen con ninguna de esas dos condiciones. Finalmente se utiliza un último pipeline y el comando wc -l para contar la cantidad de líneas en pantalla que cumplen con los patrones impuestos por el grep y se publican los resultados con el comando echo.
En caso de que la ruta pasada no sea un directorio, imprimirá en consola que no lo es y terminará el proceso de ejecución, devolviendo un 1. Para el caso de ser un directorio imprimirá en consola que el argumento ingresado es un directorio y proseguirá a contar el contenido que posea, ya sean directorios, archivos regulares y no regulares, arrojando el valor de la cantidad de cada uno que se encuentre. Una vez realizada la tarea se termina el proceso y se devuelve 0 como valor de éxito.

# TP 2: Ejercicio 6 - Resumen

El programa es un ejecutable que realiza diferentes tareas cuando se les mandan distintas señales:
- SIGUSR1: El proceso padre crea un proceso hijo que imprime su PID y el de su padre cada 5 segundos.
- SIGUSR2: El proceso padre crea un proceso hijo que ejecuta el comando "ls" con una función de la familia de funciones exec(). Luego, este proceso hijo finaliza automáticamente.
- SIGTERM: El proceso padre termina la ejecución de todos los procesos hijos creados a razón de uno por segundo, imprimiendo el PID de cada uno para luego finalizar su propia ejecución.

## TP 2: Ejercicio 6 - Funciones y tipos de variables utilizados
- handler_sigusr1(int), handler_sigusr2(int), handler_sigterm(int): son las declaraciones de las funciones de manejo de señales. Estas funciones se utilizan para manejar las señales SIGUSR1, SIGUSR2 y SIGTERM respectivamente.
- pid_t: tipo de datos entero con signo utilizado para representar identificadores de proceso (PID).
- fork(): llamada al sistema que crea un nuevo proceso hijo duplicando el proceso existente.
- getpid(): llamada al sistema que devuelve el identificador del proceso (PID) del proceso actual.
- getppid(): llamada al sistema que devuelve el identificador del proceso padre del proceso actual.
- signal(): función que establece una función para manejar una señal, es decir, un manejador de señales con un número de señal.
- printf(): función utilizada para la salida formateada a la pantalla.
- sleep(): función que hace que el proceso actual se suspenda durante un número específico de segundos.
- execl(): función que reemplaza el programa actual con un nuevo programa.
- exit(): función que termina el programa actual.
- kill(): función que envía una señal a un proceso específico.
- wait(): función que hace que el proceso actual espere hasta que uno de sus procesos hijos termine.

## TP2: Ejercicio 6 - Instrucciones de uso

1. Abrir el ejecutable en consola.
2. Abrir otra consola con Ctrl+Alt+T.
3. Usar comando ps u para ver los procesos en ejecución y ver el PID del programa que me interesa de ser necesario.
4. Mandar las señales de a una: KILL -SIGUSR1 PID (crea los hijos, para crear varios, mandar más de una señal de esta), KILL -SIGUSR2 PID (crea otro hijo que ejecuta un programa), KILL -SIGTERM PID (termina los procesos hijos creados con SIGUSR1 de a uno y al finalizar termina con el padre).
5. Fin del programa.

## TP2: Ejercicio 6 - Funcionamiento

En la consola que se realizó la ejecución del programa, este imprimirá el PID del padre y esperará que el usuario desde otra consola le envíe una señal. Al enviarle la señal SIGUSR1 con el PID del padre, este creará un proceso hijo, el cual imprimirá en pantalla su PID y el del padre cíclicamente cada 5 segundos; si se le vuelve a enviar la misma señal varias veces, se seguirán creando más hijos (tope del programa 100). Esto se realizó mediante la implementación de un  manejador de señal y un flag (flag1) o bandera para verificar que se recibió la señal enviada (en este caso SIGUSR1, handler_sigusr1). Cada vez que se reciba la señal SIGUSR1 mediante la función handler_sigusr1 se cambiará el valor de flag1 a 1. Implementado en una declaración if e imponiendo una condición a cumplir, si flag1=1 significa que se recibió la señal (luego se vuelve a reestablecer flag1=0) y se procede con la creación del proceso hijo utilizando el comando fork() (var=fork()). En caso de que el valor arrojado por fork() sea de -1, el programa imprimirá un error en consola y terminará su ejecución con un estado de fallo, mientras que si el valor devuelto es distinto de cero (var != 0) verifica que el código se está ejecutando en el proceso padre y le devolverá el PID del hijo. Por último, si fork() devuelve un valor igual a cero (var == 0) se sabe que se está ejecutando el proceso hijo donde dentro del mismo se observa el printf que imprimirá en consola el PID del mismo y del padre cada 5 segundos.
Al enviarle al proceso padre la señal SIGUSR2 se creará un nuevo proceso hijo, el cual ejecutará un programa ubicado en el directorio /bin/ (comando ‘ls’ en nuestro caso) utilizando la función execl(). Una vez realizada la tarea, este proceso hijo terminará su ejecución. Como en el caso anterior, cuando se recibe la señal se activa un flag (flag2 = 1) en el handler (SIGUSR2, handler_sigusr2) cuya condición se chequea en un if en el main para asegurarse de que sea solo este proceso el que realice la tarea.
Para concluir se debe enviar la señal SIGTERM al proceso padre, que es la encargada de terminar con todos los procesos hijos creados por la señal SIGUSR1. Los mismos se terminarán a razón de uno por segundo y cuando esto concluya, el proceso principal (padre) terminará su ejecución y se imprimirá en consola la confirmación del mismo. En el código se puede encontrar esta implementación con el manejador de señal (SIGTERM, handler_sigterm) y el flag3. Al entrar a la declaración else if con un valor de flag3==1 se ejecutará un bucle for que recorre un vector que tiene almacenados los PID de todos los procesos hijos creados y el padre irá matando o terminando cada uno de ellos con la señal kill(vector_hijos[i], SIGKILL) haciendo uso de la función sleep() para esperar un segundo entre medio. El PID de cada proceso hijo terminado se informará en consola. 
En el bucle for se introdujo el comando wait() como medida de seguridad, que hace que el padre espere a que la ejecución del hijo termine para continuar con su propia ejecución. Para concluir, una vez terminados todos los procesos hijo se terminará con el proceso padre y se confirmara que el programa principal ha finalizado con un mensaje en consola.





